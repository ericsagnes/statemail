<?php

class StateMailType extends eZWorkflowEventType
{
  const WORKFLOW_TYPE_STRING = "statemail";
  public function __construct()
  {
    parent::__construct( StateMailType::WORKFLOW_TYPE_STRING, 'State Mail' );
  }

  public function execute( $process, $event )
  {
    $parameters = $process->attribute( 'parameter_list' );

    
    // ステートは期待通りのステートの場合にメールを送信
    $object_states = $parameters['state_id_list'];

    // メールを送信するステート
    // 設定ファイルに移動するといいです
    $mail_state = "4";

    // オブジェクトはメールを送るステートを持てるのを確認し、メールを送信します
    if ( in_array($mail_state, $object_states) ){
      // オブジェクトをフェッチします
      $object_id = $parameters['object_id'];
      $object = eZContentObject::fetch( $object_id );

      // publish_dateフィールドの値を更新
      $data_map = $object->attribute('data_map');
      $publish_date = $data_map['publish_date'];
      $publish_date->setAttribute( 'data_int', time() );
      $publish_date->sync();

      // メール送信ファンクションの呼び出し
      $this->send_mail( $object );
    }

    return eZWorkflowType::STATUS_ACCEPTED;
  }

  public function send_mail( $object )
  {
    $data_map = $object->attribute('data_map');
    // $datamap はオブジェクトの属性の情報を持っています
    // title属性の値はかきのようで出せます
    // $data_map['title']->attribute('content')

    // メールを送信するロジック
    // ezcomponentsを使うとやりやすい
    // http://ezcomponents.org/docs/tutorials/Mail

    $mail = new ezcMailComposer();
    
    // メールアドレスなどは設定ファイルに移動するといいです
    $mail->from = new ezcMailAddress( 'sender@example.com', '送信者の名前' );
    $mail->addTo( new ezcMailAddress( 'receiver@example.com', '受信者の名前' ) );
    $mail->subject = "オブジェクトのステート変更報告";

    $object_name = $object->attribute('name');
    $mail->plainText = "オブジェクト[" . $object_name . "]は変更されました。";

    $mail->build();
    // SMTPも利用可能
    // http://ezcomponents.org/docs/tutorials/Mail#sending-a-mail-using-smtp
    $transport = new ezcMailMtaTransport();
    try {
      $transport->send( $mail );
    } catch (Exception $e) {
      echo $e->getMessage();
      return false;
    }
    return true;
  }
}

eZWorkflowEventType::registerEventType( StateMailType::WORKFLOW_TYPE_STRING, 'statemailtype' );

?>
